﻿using AusPitsSupplyChainSupportingServices.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AusPitsSupplyChainSupportingServices.Helpers;
using Microsoft.SharePoint.Client;

namespace AusPitsSupplyChainSupportingServices.Controllers
{
    public class SupplyChainProjectController : ApiController
    {
        string ProjectFolderBasePath;
        string ProjectFolderBaseURL;
        SupplyChainProject project = new SupplyChainProject();

        public SupplyChainProjectController()
        {
            ProjectFolderBasePath = System.Configuration.ConfigurationManager.AppSettings["ProjectFolderBasePath"].ToString();
            ProjectFolderBaseURL = System.Configuration.ConfigurationManager.AppSettings["ProjectFolderBaseURL"].ToString();
        }        


        // This action is launched by AusPits CSA staff when a new project is entered/created in the SP Supply Chain Projects website
        // The action creates the folder structure required to store project assets
        [HttpGet]
        public string OnBoardSupplyChainProject(int ProjectID)
        {
            SharePointHelpers sp = new SharePointHelpers();
            return sp.OnBoardChainProject(ProjectID, @ProjectFolderBasePath, ProjectFolderBaseURL);
        }

        
        [HttpGet]
        public string GetProject(int ProjectID)
        {
            SharePointHelpers sp = new SharePointHelpers();
            ListItem item = sp.GetSupplyChainProject(ProjectID);

            return "ok";
        }


        [HttpGet]
        public string SynchroniseQuoteSystemClientList()
        {
            SharePointHelpers sp = new SharePointHelpers();
            string result = sp.SynchroniseQuoteSystemClientList();

            return result;
        }

        [HttpGet]
        public string CreateQuoteSystemClient(string CustomerName, string ContactName, string Phone, string Mobile, string Email)
        {
            QuoteSystemHelpers qs = new QuoteSystemHelpers();
            string result = qs.CreateQuoteSystemClient(CustomerName, ContactName, Phone, Mobile, Email);

            return result;
        }
    }
}
