﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AusPitsSupplyChainSupportingServices.Helpers
{
    public class QuoteSystemHelpers
    {

        public string CreateQuoteSystemClient(string CustomerName, string ContactName, string Phone, string Mobile, string Email)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["QuoteSystemEntities"].ToString()))
                {
                    // Ensure CustomerName does not already exist
                    string queryString = string.Concat("SELECT COUNT(*) FROM AdminCustomer WHERE [CustomerName] = '", CustomerName, "'");
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    Int32 recCount = Convert.ToInt32(command.ExecuteScalar());
                    if (recCount == 0)
                    {
                        // Create the new customer
                        string sql = "INSERT INTO AdminCustomer (CustomerName, ContactName, Phone, Mobile, Email, Active) VALUES (@CustomerName, @ContactName, @Phone, @Mobile, @Email, @Active)";
                        SqlCommand insCommand = new SqlCommand(sql, connection);
                        insCommand.Parameters.AddWithValue("@CustomerName", CustomerName);
                        insCommand.Parameters.AddWithValue("@ContactName", ContactName);
                        insCommand.Parameters.AddWithValue("@Phone", Phone);
                        insCommand.Parameters.AddWithValue("@Mobile", Mobile);
                        insCommand.Parameters.AddWithValue("@Email", Email);
                        insCommand.Parameters.AddWithValue("@Active", 1);
                        
                        insCommand.ExecuteNonQuery();

                        return "ok.";
                    }
                    else
                    {
                        return "Customer already exists";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}