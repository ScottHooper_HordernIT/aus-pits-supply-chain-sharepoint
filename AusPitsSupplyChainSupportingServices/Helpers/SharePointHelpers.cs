﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Microsoft.SharePoint.Client;
using System.Globalization;
using System.Security;
using System.Threading;
using System.IO;
using System.Data.SqlClient;

namespace AusPitsSupplyChainSupportingServices.Helpers
{
    public class SharePointHelpers
    {
        ClientContext clientContext;

        public SharePointHelpers()
        {
            
        }

        ClientContext GetContext(string url, string username, string password)
        {
            var ctx = new ClientContext(url);
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                var securePassword = new SecureString();
                foreach (char c in password) securePassword.AppendChar(c);
                ctx.Credentials = new SharePointOnlineCredentials(username, securePassword);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return ctx;
        }

        public ListItem GetSupplyChainProject(int ItemID)
        {
            clientContext = GetContext(ConfigurationManager.AppSettings["SharePointLink"],
                    ConfigurationManager.AppSettings["Username"],
                    ConfigurationManager.AppSettings["Password"]);

            ListItemCollection items;

            try
            {
                var list = clientContext.Web.Lists.GetByTitle(ConfigurationManager.AppSettings["SPSupplyChainProjectsListName"]);
                var camlQuery = CamlQuery.CreateAllItemsQuery();
                camlQuery.ViewXml = "<View><Query><Where>" +
                                "<Eq>" +
                                    "<FieldRef Name='ID' />" +
                                    "<Value Type='Number'><ITEMID></Value>" +
                                "</Eq>" +
                            " </Where>" +
                            "</Query>" +
                            "</View>";
                camlQuery.ViewXml = camlQuery.ViewXml.Replace("<ITEMID>", ItemID.ToString());
                items = list.GetItems(camlQuery);
                clientContext.Load(items);
                clientContext.ExecuteQuery();
            }
            catch (Exception ex)
            {
                return null;
            }

            if (items.Count > 0)
            {
                return items[0];
            } else
            {
                return null;
            }
        }

        public string OnBoardChainProject(int ItemID, string ProjectFolderBasePath, string ProjectFolderBaseURL)
        {
            try
            {
                clientContext = GetContext(ConfigurationManager.AppSettings["SharePointLink"],
                    ConfigurationManager.AppSettings["Username"],
                    ConfigurationManager.AppSettings["Password"]);

                List list = clientContext.Web.Lists.GetByTitle(ConfigurationManager.AppSettings["SPSupplyChainProjectsListName"]);
                ListItem listItem = list.GetItemById(ItemID);
                clientContext.Load(listItem);
                clientContext.ExecuteQuery();

                //foreach (Field field in list.Fields)
                //{
                //    clientContext.Load(field);
                //    clientContext.ExecuteQuery();
                //}

                string projectName, clientName;
                FieldLookupValue clientLookup = listItem["Client_x0020_Name0"] as FieldLookupValue;
                clientName = clientLookup.LookupValue;
                projectName = listItem["Title"].ToString();
                string ProjectFolderPath = string.Concat(clientName, " - ", projectName, " (", ItemID.ToString(), ")");

                FieldUrlValue urlValue = new FieldUrlValue();
                urlValue.Description = "Project Folder";
                urlValue.Url = string.Concat(ProjectFolderBaseURL, ProjectFolderPath);

                listItem["Project_x0020_Folder_x0020_URL"] = urlValue;
                listItem["Project_x0020_Notes_x0020_URL"] = urlValue;
                listItem.Update();
                clientContext.Load(listItem);
                clientContext.ExecuteQuery();

                
                DirectoryInfo directoryInfo = Directory.CreateDirectory(@ProjectFolderBasePath + ProjectFolderPath);
                if (directoryInfo != null)
                {
                    directoryInfo.CreateSubdirectory("Correspondence");
                    directoryInfo.CreateSubdirectory("Current Drawings");
                    directoryInfo.CreateSubdirectory("Production Cover Sheets");
                    directoryInfo.CreateSubdirectory("Shop Cards");
                    directoryInfo.CreateSubdirectory("Variations, Purchase Orders, Contracts");
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "ok";
        }

        public string SynchroniseQuoteSystemClientList()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["QuoteSystemEntities"].ToString()))
                {
                    // Load Customers from Quote System
                    string queryString = "SELECT [CustomerName], [ContactName] FROM AdminCustomer";
                    SqlCommand command = new SqlCommand(queryString, connection);
                    //command.Parameters.AddWithValue("@tPatSName", "Your-Parm-Value");
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Load Customers from SharePoint
                    clientContext = GetContext(ConfigurationManager.AppSettings["SharePointLink"],
                    ConfigurationManager.AppSettings["Username"],
                    ConfigurationManager.AppSettings["Password"]);

                    List list = clientContext.Web.Lists.GetByTitle(ConfigurationManager.AppSettings["SPSupplyChainCustomersListName"]);

                    // This creates a CamlQuery that has a RowLimit of 100, and also specifies Scope="RecursiveAll" 
                    // so that it grabs all list items, regardless of the folder they are in. 
                    CamlQuery query = CamlQuery.CreateAllItemsQuery();
                    ListItemCollection items = list.GetItems(query);

                    clientContext.Load(items);
                    clientContext.ExecuteQuery();

                    try
                    {
                        while (reader.Read())
                        {
                            string ClientName = reader[0].ToString();
                            string ContactName = reader[1].ToString();
                            bool found = false;

                            foreach (ListItem listItem in items)
                            {                                
                                if (listItem.FieldValues["Company"] != null)
                                {
                                    if (listItem.FieldValues["Company"].ToString() == ClientName)
                                    {
                                        found = true;
                                    }
                                }
                            }

                            if (!found)
                            {
                                // new client in Quote System, will need to be added to SharePoint
                                ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                                ListItem newItem = list.AddItem(itemCreateInfo);
                                newItem["Title"] = ContactName;
                                newItem["Company"] = ClientName;
                                newItem.Update();

                                clientContext.ExecuteQuery();
                            }

                            
                            //ListItem item = items.Where(a => a.Client_Title == ClientName).SingleOrDefault();
                        }
                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "ok";
        }
    }
}